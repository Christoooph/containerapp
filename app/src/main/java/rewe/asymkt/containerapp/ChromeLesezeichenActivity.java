package rewe.asymkt.containerapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class ChromeLesezeichenActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<ShortcutItem> shortcutlistLesezeichen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrome_lesezeichen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        shortcutlistLesezeichen = new ArrayList<ShortcutItem>();
        shortcutlistLesezeichen.add(new ShortcutItem("Inet","chrome","https://inet"));
        shortcutlistLesezeichen.add(new ShortcutItem("RGM","chrome","https://inet"));
        shortcutlistLesezeichen.add(new ShortcutItem("Checklistentool","chrome","https://inet"));
        shortcutlistLesezeichen.add(new ShortcutItem("Aufgabentool","chrome","https://inet"));
        shortcutlistLesezeichen.add(new ShortcutItem("Flugblatt","chrome","https://inet"));
        shortcutlistLesezeichen.add(new ShortcutItem("Plakate","chrome","https://inet"));

        recyclerView = findViewById(R.id.recycleVariante);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        ShortcutAdapter sAdapter = new ShortcutAdapter(shortcutlistLesezeichen);
        recyclerView.setAdapter(sAdapter);

        sAdapter.setOnItemClickListener(new RecyclerViewItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                switch(shortcutlistLesezeichen.get(position).getType()){
                    case "chrome":
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(shortcutlistLesezeichen.get(position).getUrltarget()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setPackage("com.android.chrome");
                        try {
                            // mit Chrome gestartet
                            startActivity(i);
                        } catch (ActivityNotFoundException e) {
                            // Chrome is probably not installed
                            // Try with the default browser
                            i.setPackage(null);
                            startActivity(i);
                        }
                        break;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
