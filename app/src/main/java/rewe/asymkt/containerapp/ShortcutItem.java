package rewe.asymkt.containerapp;

import android.graphics.Color;

public class ShortcutItem {
    private String name; // angezeigter name
    private String type; // typ

    // Anzeige
    private String bgcolor;
    private String txtcolor;

    // chrome
    private String urltarget;

    public ShortcutItem(String name, String type){
        this.name = name;
        this.type = type;
        urltarget="";
        bgcolor = null;
        txtcolor = null;
    }

    public ShortcutItem(String name, String type, String url){
        this.name = name;
        this.type = type;
        urltarget=url;
        bgcolor = null;
        txtcolor = null;
    }

    public ShortcutItem(String name, String type, String url, String txtcolor, String bgcolor){
        this.name = name;
        this.type = type;
        this.urltarget=url;
        this.bgcolor = bgcolor;
        this.txtcolor = txtcolor;
    }

    public ShortcutItem(String name, String type, String txtcolor, String bgcolor){
        this.name = name;
        this.type = type;
        this.urltarget="";
        this.bgcolor = bgcolor;
        this.txtcolor = txtcolor;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getTxtcolor() {
        return txtcolor;
    }

    public void setTxtcolor(String txtcolor) {
        this.txtcolor = txtcolor;
    }

    public String getUrltarget() {
        return urltarget;
    }

    public void setUrltarget(String urltarget) {
        this.urltarget = urltarget;
    }
}
