package rewe.asymkt.containerapp;

import android.view.View;

public interface RecyclerViewItemClickListener{
    void onItemClick(View view, int position);
}