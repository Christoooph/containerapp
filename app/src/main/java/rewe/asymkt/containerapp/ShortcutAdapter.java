package rewe.asymkt.containerapp;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import rewe.asymkt.containerapp.R;


public class ShortcutAdapter extends RecyclerView.Adapter<ShortcutAdapter.ViewHolder> {


    public ArrayList<ShortcutItem> item_list;
    private RecyclerViewItemClickListener recyclerViewItemClickListener;


    public ShortcutAdapter(ArrayList<ShortcutItem> arrayList) {
        item_list = arrayList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView name;
        public View strich;
        public LinearLayout layout;

        public int position = 0;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            image = (ImageView) itemLayoutView.findViewById(R.id.simage);
            name = (TextView) itemLayoutView.findViewById(R.id.stext);
            layout = (LinearLayout) itemLayoutView.findViewById(R.id.slinearlayout);
            strich = (View) itemLayoutView.findViewById(R.id.sstrich);
            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //When item view is clicked, trigger the itemclicklistener
                    //Because that itemclicklistener is indicated in MainActivity
                    recyclerViewItemClickListener.onItemClick(v,position);
                    // code
                }
            });
        }
    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener recyclerViewItemClickListener){
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

    @Override
    public ShortcutAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ShortcutAdapter.ViewHolder holder, int position) {

        final int pos = position;

        holder.position = position;

        holder.name.setText(item_list.get(position).getName());
        switch(item_list.get(position).getType()){
            case "chromeWeiterleitung": case "chrome": holder.image.setImageResource(R.drawable.chrome); break;
            case "cam": holder.image.setImageResource(R.drawable.icon_cam); break;
            case "chat": holder.image.setImageResource(R.drawable.icon_chat); break;
            case "phone": holder.image.setImageResource(R.drawable.icon_phone); break;
            case "temp": holder.image.setImageResource(R.drawable.heaticon); break;
        }

        if(item_list.get(position).getBgcolor() != null) holder.layout.setBackgroundColor(Color.parseColor(item_list.get(position).getBgcolor()));
        else holder.layout.setBackgroundColor(Color.TRANSPARENT);
        if(item_list.get(position).getTxtcolor() != null) {
            holder.name.setTextColor(Color.parseColor(item_list.get(position).getTxtcolor()));
            holder.strich.setBackgroundColor(Color.parseColor(item_list.get(position).getTxtcolor()));
        }
    }

    @Override
    public int getItemCount() {
        return item_list.size();
    }
}