package rewe.asymkt.containerapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ContainerAPPActivity extends AppCompatActivity{
    RecyclerView recyclerView;
    ArrayList<ShortcutItem> shortcutlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container_app);


        shortcutlist = new ArrayList<ShortcutItem>();
        //shortcutlist.add(new ShortcutItem("Lesezeichen","chromeWeiterleitung"));
        shortcutlist.add(new ShortcutItem("Inet","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("RGM","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("Checklistentool","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("Aufgabentool","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("Flugblatt","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("Plakate","chrome","https://inet"));
        shortcutlist.add(new ShortcutItem("Temperaturkontrolle","temp"));
        shortcutlist.add(new ShortcutItem("Kamera","cam", "#ffffff", "#39ac71"));
        shortcutlist.add(new ShortcutItem("Telefonbuch","phone", "#ffffff", "#39ac71"));
        shortcutlist.add(new ShortcutItem("Chat","chat", "#ffffff", "#39ac71"));

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        ShortcutAdapter sAdapter = new ShortcutAdapter(shortcutlist);
        recyclerView.setAdapter(sAdapter);

        sAdapter.setOnItemClickListener(new RecyclerViewItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                switch(shortcutlist.get(position).getType()){
                    case "chrome":
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(shortcutlist.get(position).getUrltarget()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setPackage("com.android.chrome");
                        try {
                            // mit Chrome gestartet
                            startActivity(i);
                        } catch (ActivityNotFoundException e) {
                            // Chrome is probably not installed
                            // Try with the default browser
                            i.setPackage(null);
                            startActivity(i);
                        }
                        break;

                    // Variante
                    case "chromeWeiterleitung":
                        Intent intent = new Intent(view.getContext(), ChromeLesezeichenActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });

        ImageView vinfo = (ImageView)findViewById(R.id.imageViewInfo);
        vinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), InfoActivity.class);
                startActivity(intent);
            }
        });

        ImageView vsettings = (ImageView)findViewById(R.id.imageViewSettings);
        vsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });
    }
}
